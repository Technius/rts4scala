import Dependencies._
import AdditionalSettings._

lazy val root = (project in file("."))
  .settings(
    inThisBuild(List(
      organization := "co.technius",
      scalaVersion := "2.11.7",
      version      := "0.1.0-SNAPSHOT",
      libraryDependencies += scalaTest % Test,
      assetsDir := (baseDirectory in ThisBuild).value / "assets"
    )),
    run in Compile := (run in Compile in desktop).evaluated,
    name := "rts4scala"
  )
  .aggregate(game, engine, desktop)

lazy val engine = (project in file("engine"))
  .settings(
    unmanagedResourceDirectories in Compile += assetsDir.value
  )

lazy val game = (project in file("game"))
  .settings(
    libraryDependencies ++= libGdxLibs
  )
  .dependsOn(engine)

lazy val desktop = (project in file("desktop"))
  .settings(
    libraryDependencies ++= libGdxLibs ++ libGdxDesktopLibs,
    fork in run := true
  )
  .dependsOn(game)
