import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"

  val libGdxVersion = "1.9.5"
  lazy val libGdxLibs = Seq(
    "com.badlogicgames.gdx" % "gdx" % libGdxVersion,
    "com.badlogicgames.gdx" % "gdx-freetype" % libGdxVersion
  )
  lazy val libGdxDesktopLibs = Seq(
    "com.badlogicgames.gdx" % "gdx-platform" % libGdxVersion classifier "natives-desktop",
    "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % libGdxVersion,
    "com.badlogicgames.gdx" % "gdx-freetype-platform" % libGdxVersion classifier "natives-desktop"
  )
}
