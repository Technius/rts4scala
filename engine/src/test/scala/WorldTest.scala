import org.scalatest._

import co.technius.rts4scala.engine.World

class WorldTest extends FlatSpec with Matchers {

  def checkDistinct(world: World) = {
    val allIds = world.getAllEntities.map(_.id)
    allIds.distinct.size should === (allIds.size)
  }

  "A world" should "properly allocate entities" in {
    val world = new World
    val e = world.newEntity()
    val e2 = world.newEntity()
    e.id should === (0)
    e2.id should === (1)
  }

  it should "properly deallocate entities" in {
    val world = new World
    for (_ <- 1 to 10) world.newEntity()
    world.getEntity(0) should not be (None)
    world.removeEntity(0)
    world.getEntity(0) should be (None)
  }

  it should "reuse entity ids" in {
    val world = new World
    for (_ <- 1 to 10) world.newEntity()
    world.removeEntity(5)
    world.getEntity(5) should be (None)
    val e = world.newEntity()
    world.getEntity(5) should not be (None)
  }

  it should "not contain entities with duplicate ids" in {
    val world = new World
    for (_ <- 1 to 1000) world.newEntity()

    checkDistinct(world)
    for (id <- 0 to 500) world.removeEntity(id)
    checkDistinct(world)
  }

  it should "ignore removal of nonexistent entities" in {
    val world = new World
    for (_ <- 1 to 10) world.newEntity()
    world.removeEntity(1234) should be (None)
    world.removeEntities(11, 121, 123) should be ('empty)
    world.getAllEntities should have size (10)
    checkDistinct(world)
  }
}
