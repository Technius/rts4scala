package co.technius.rts4scala.engine

import scala.collection.immutable.Seq
import scala.reflect.ClassTag

/**
  * Emits events that determine the changes that should be applied to the given
  * entities.
  * Explicitly NOT a (Seq[Entity] => Event) since a system may use effectful
  * code to do caching, optimizations, etc.
  */
trait EntitySystem {
  def handle(entities: Seq[Entity]): Seq[Event]
}

object EntitySystem {

  /**
    * Creates an EntitySystem from the given function
    */
  def apply(f: Seq[Entity] => Seq[Event]): EntitySystem =
    new EntitySystem {
      def handle(entities: Seq[Entity]): Seq[Event] = f(entities)
    }

  /**
    * Creates an EntitySystem that generates events from each entity
    */
  def iterate(f: Entity => Seq[Event]): EntitySystem =
    new EntitySystem {
      def handle(entities: Seq[Entity]): Seq[Event] = entities.flatMap(f)
    }

  /**
    * Creates an EntitySystem that generates events given the entities with the
    * specified component
    */
  def component[C <: Component: ClassTag](f: (Entity, C) => Seq[Event]): EntitySystem =
    new EntitySystem {
      def handle(entities: Seq[Entity]): Seq[Event] = {
        entities flatMap { e =>
          e.getComponent[C] match {
            case Some(c) => f(e, c)
            case None => Seq.empty[Event]
          }
        }
      }
    }

  /**
    * Creates an EntitySystem that generates events given the entities with the
    * specified components
    */
  def component2[C <: Component: ClassTag, D <: Component: ClassTag](f: (Entity, C, D) => Seq[Event]): EntitySystem =
    new EntitySystem {
      def handle(entities: Seq[Entity]): Seq[Event] = {
        entities flatMap { e =>
          val opt = for {
            c <- e.getComponent[C]
            d <- e.getComponent[D]
          } yield (c, d)
          opt match {
            case Some((c, d)) => f(e, c, d)
            case None => Seq.empty[Event]
          }
        }
      }
    }

  /**
    * An EntitySystem that does not generate any events.
    */
  def nothing = apply(_ => Seq())
}
