package co.technius.rts4scala.engine

sealed trait Component
object Component {
  case class Health(var value: Int) extends Component
  case class Position(var x: Int, var y: Int) extends Component
  case class Velocity(var x: Int, var y: Int) extends Component
}
