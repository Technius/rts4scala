package co.technius.rts4scala.engine

import scala.collection.mutable.{ ArrayBuffer, ListBuffer, Queue }

/**
  * Manages entity creation and destruction, as well as system processing and
  * event routing.
  */
class World {
  private[this] val entities = ArrayBuffer[Entity]()
  private[this] val unassignedIds = Queue[Int](1 to entities.length: _*)

  private[this] def idExists(id: Int): Boolean = id < entities.size && id >= 0

  /**
    * Creates a new entity using an unassigned id. May cause the internal entity
    * collection to expand if it is not large enough to contain additional
    * entities.
    */
  def newEntity(): Entity = {
    if (unassignedIds.isEmpty) {
      val id = entities.length
      val e = new Entity(id)
      entities.append(e)
      e
    } else {
      val id = unassignedIds.dequeue()
      val e = new Entity(id)
      entities(id) = e
      e
    }
  }

  /**
    * If it exists, the entity with the specified id is removed and returned.
    */
  def removeEntity(id: Int): Option[Entity] = {
    if (!idExists(id)) {
      None
    } else {
      val e = entities(id)
      if (e == null) {
        None
      } else {
        unassignedIds += id
        entities(id) = null
        Some(e)
      }
    }
  }

  def removeEntities(ids: Int*): List[Entity] = {
    val buffer = new ListBuffer[Entity]()
    for(id <- ids if idExists(id)) {
      val e = entities(id)
      if (e != null) {
        buffer.prepend(e)
      }
    }
    val es = buffer.toList
    unassignedIds ++= es.map(_.id)
    es
  }

  /**
    * Optionally retrieves the entity with the specified id.
    * If the entity is known to exist, getEntityUnsafe is faster for retrieval.
    */
  def getEntity(id: Int): Option[Entity] =
    if (!idExists(id)) None else Option(entities(id))

  /**
    * Retrieves the entity with the specified id in constant time, but may
    * return null or throw an exception if no such entity exists. Use only if the
    * entity is known to exist or to minimize garbage collection in
    * performance-critical situations.
    */
  def getEntityUnsafe(id: Int): Entity = entities(id)

  /**
    * Returns an (immutable) [[scala.Vector]] containing all entities tracked by
    * this World. Note that the entities themselves are not immutable.
    */
  def getAllEntities: Vector[Entity] = entities.view.filter(_ != null).toVector
}
