package co.technius.rts4scala.engine

import scala.language.implicitConversions

final case class Event(action: Action, target: EventTarget)

object Event {
  implicit def tupleToEvent(e: (Action, EventTarget)): Event =
    Event(e._1, e._2)
}

sealed trait Action
object Action {
  case class Damage(amount: Int) extends Action
  case class Movement(vx: Int, vy: Int) extends Action
  case object Die extends Action
}

sealed trait EventTarget
object EventTarget {
  case class Entity(id: Long) extends EventTarget
  case object World extends EventTarget
}
