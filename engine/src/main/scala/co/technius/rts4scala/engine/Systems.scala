package co.technius.rts4scala.engine

import scala.collection.immutable.Seq

object Systems {
  private[this] val C = Component
  def movementSystem =
    EntitySystem.component2 { (e, pos: C.Position, v: C.Velocity) =>
      Seq.empty[Event]
    }

  def deathSystem =
    EntitySystem.component { (e, health: C.Health) =>
      if (health.value <= 0) Seq(Action.Die -> EventTarget.Entity(e.id))
      else Seq()
    }
}
