package co.technius.rts4scala.engine

import scala.collection.mutable.HashMap
import scala.reflect.ClassTag

/**
  * An Entity with an id and components.
  * The id is stored in the entity for fast lookups.
  * Components are stored by class in a hash map for fast lookups.
  */
class Entity private[engine](val id: Int) {
  private[this] val _components = HashMap[Class[_], Component]()

  /**
    * Forcibly sets the component, regardless if the component already exists.
    */
  def updateComponent[C <: Component](value: C): Unit =
    _components(value.getClass) = value

  /**
    * Adds the component if the entity does not already contain a component of
    * the same class.
    */
  def addComponent[C <: Component](value: C): Boolean = {
    if (_components.isDefinedAt(value.getClass)) {
      false
    } else {
      _components(value.getClass) = value
      true
    }
  }

  /**
    * Optionally retrieves the component.
    */
  def getComponent[S <: Component](implicit ct: ClassTag[S]): Option[S] =
    _components.get(ct.runtimeClass).map(_.asInstanceOf[S])

  /**
    * Retrieves the component or uses the default value if the entity does not
    * contain the component of the target type.
    */
  def getComponentOrSet[S <: Component](default: => S)(implicit ct: ClassTag[S]): S =
    _components.getOrElseUpdate(ct.runtimeClass, default).asInstanceOf[S]

  /**
    * Returns an (immutable) vector containing the components. Note that the
    * components themselves may not be immutable.
    */
  def components: Vector[Component] =
    _components.values.toVector
}
