package co.technius.rts4scala.desktop

import com.badlogic.gdx.backends.lwjgl.{ LwjglApplication, LwjglApplicationConfiguration }

import co.technius.rts4scala.game.Rts4ScalaGame

object Rts4ScalaDesktop {
  def main(args: Array[String]): Unit = {
    val cfg = new LwjglApplicationConfiguration()
    cfg.title = "RTS4Scala"
    cfg.width = 800
    cfg.height = 600
    cfg.forceExit = false
    // ugh, instantiation causes a side effect :/
    new LwjglApplication(new Rts4ScalaGame, cfg)
  }
}
