package co.technius.rts4scala.game

import com.badlogic.gdx.{ Game, Gdx, Screen }
import com.badlogic.gdx.graphics.{ Color, GL20 }
import com.badlogic.gdx.graphics.g2d.{ BitmapFont, GlyphLayout, SpriteBatch }
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.{ InputListener, InputEvent, Stage }
import com.badlogic.gdx.scenes.scene2d.ui

class Rts4ScalaGame extends Game {
  override def create(): Unit = {
    setScreen(new DefaultScreen)
  }
}

class DefaultScreen extends Screen {
  val skin = new ui.Skin(Gdx.files.internal("vis-skin/skin/x1/uiskin.json"))

  val stage = new Stage
  Gdx.input.setInputProcessor(stage)

  var debugging = false
  val table = new ui.Table(skin)
  table.setFillParent(true)
  val helloMsg = new ui.Label("Hello world!", skin)
  val stupidBtn = new ui.TextButton("Click me!", skin)
  val btn2 = new ui.TextButton("Yo", skin)
  val debugBtn = new ui.TextButton("Toggle debug", skin)
  table.add(helloMsg)
  table.add(stupidBtn)
  table.row()
  table.add(btn2)
  table.row()
  table.add(debugBtn)

  debugBtn.addListener(new InputListener {
    override def touchDown(event: InputEvent, x: Float, y: Float, ptr: Int, btn: Int): Boolean = {
      debugging = !debugging
      table.setDebug(debugging)
      true
    }
  })

  stage.addActor(table)

  override def render(dt: Float): Unit = {
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    stage.act(dt)
    stage.draw()
  }

  override def dispose(): Unit = {
    stage.dispose()
  }

  def resize(w: Int, h: Int): Unit = {
    stage.getViewport().update(w, h, true)
  }

  def hide(): Unit = ()
  def pause(): Unit = ()
  def resume(): Unit = ()
  def show(): Unit = ()
}
